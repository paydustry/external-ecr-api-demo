package com.paydustry.register.aidl;

import android.os.Parcel;
import android.os.Parcelable;

public enum AlignAttribute implements Parcelable {
    left, center, right
    ;

    public static final Creator<AlignAttribute> CREATOR = new Creator<AlignAttribute>() {
        @Override
        public AlignAttribute createFromParcel(Parcel in) {
            return AlignAttribute.fromInteger(in.readInt());
        }

        @Override
        public AlignAttribute[] newArray(int size) {
            return new AlignAttribute[size];
        }
    };
    
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(toInteger());
    }

    public int toInteger() { return this.ordinal(); }
    public static AlignAttribute fromInteger(int value) {
        return values()[value];
    }
}
