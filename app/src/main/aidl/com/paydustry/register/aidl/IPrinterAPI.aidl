package com.paydustry.register.aidl;

import com.paydustry.register.aidl.ReceiptItem;
import com.paydustry.register.aidl.PrinterStatus;

interface IPrinterAPI {

    /**
     * Print the given list of the Receipt Items on receipt adding feed at the end
     * Param list should be one of: ImageItem or TextItem
     * @param receiptItemList
     * @throws RemoteException
     */
    void printWithFeed(in java.util.List<ReceiptItem> receiptItemList);

    /**
     * Print the given list of the Receipt Items on receipt not adding feed at the end.
     * The only difference between "printWithFeed" is, this api is not printing feed.
     * Param list should be one of: ImageItem or TextItem
     * @param receiptItemList
     * @throws RemoteException
     */
    void print(in java.util.List<ReceiptItem> receiptItemList);

    /**
     * Get the current status of the printer.
     * Would there be a paper end for the printer, waitForPrinterReady api should be called
     * in order to check if the customer re-fills the printer with new receipt.
     * @return
     * @throws RemoteException
     */
    PrinterStatus getPrinterStatus();

    /**
     * Waits until PrinterStatus.equals(OK)
     * @throws RemoteException
     */
    void waitForPrinterReady();
}
