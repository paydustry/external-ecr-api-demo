package com.paydustry.register.aidl;

import com.paydustry.register.aidl.IPrinterAPI;
import com.paydustry.register.aidl.ISmartCardAPI;

interface IRegisterAPI {

   /**
     * Retrieve the printer interface API object to use printer functionality.
     * @return IPrinterAPI
     */
    IPrinterAPI getPrinter();

   /**
     * Retrieve the smartCard interface API object to use PSAM functionality.
     * @return ISmartCardAPI
     */
    ISmartCardAPI getSmartCard();
}
