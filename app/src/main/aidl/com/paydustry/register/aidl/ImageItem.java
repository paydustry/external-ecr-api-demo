package com.paydustry.register.aidl;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class ImageItem extends ReceiptItem {
    /**
     * Image to print on receipt.
     * Use this api to print image.
     * @param data (Bitmap) to be printed
     * @param alignAttribute to align the text
     * @param sizeAttribute to give the size
     * @param weightAttribute to set if the text should be bold
     */
    public ImageItem(Bitmap data, AlignAttribute alignAttribute, SizeAttribute sizeAttribute, WeightAttribute weightAttribute) {
        super(ImageItem.class, "", alignAttribute, sizeAttribute, weightAttribute);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        data.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] buffer = stream.toByteArray();
        this.data = byte2hex(buffer);
    }
    /**
     * Image to print on receipt.
     * Use this api to print image.
     * @param data (InputStream) to be printed
     * @param alignAttribute to align the text
     * @param sizeAttribute to give the size
     * @param weightAttribute to set if the text should be bold
     */
    public ImageItem(InputStream data, AlignAttribute alignAttribute, SizeAttribute sizeAttribute, WeightAttribute weightAttribute) {
        this(BitmapFactory.decodeStream(new BufferedInputStream(data)), alignAttribute, sizeAttribute, weightAttribute);
    }

    /**
     * Constructor to be used for internal purposes
     */
    protected ImageItem(Parcel in) {
        super(in);
    }

    /**
     * Constructor to be used for internal purposes
     */
    public ImageItem(ReceiptItem receiptItem) {
        this(getBitmap(receiptItem.data), receiptItem.alignAttribute, receiptItem.sizeAttribute, receiptItem.weightAttribute);
    }

    private static Bitmap getBitmap(String stringData) {
        byte[] bitmapdata = hex2byte(stringData);
        return BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
    }

    public static byte[] hex2byte(String s) {
        if (s == null) {
            throw new NullPointerException();
        }
        if (s.length() % 2 == 0) {
            return hex2byte(s.getBytes(), 0, s.length() >> 1);
        } else {
            // Padding left zero to make it even size #Bug raised by tommy
            return hex2byte("0" + s);
        }
    }
    public static byte[] hex2byte(byte[] b, int offset, int len) {
        byte[] d = new byte[len];
        for (int i = 0; i < len * 2; i++) {
            int shift = i % 2 == 1 ? 0 : 4;
            d[i >> 1] |= Character.digit((char) b[offset + i], 16) << shift;
        }
        return d;
    }

    private static String byte2hex(byte[] bs) {
        return byte2hex(bs, 0, bs.length);
    }

    private static String byte2hex(byte[] bs, int off, int length) {
        if (bs.length <= off || bs.length < off + length)
            throw new IllegalArgumentException();
        StringBuilder sb = new StringBuilder(length * 2);
        byte2hexAppend(bs, off, length, sb);
        return sb.toString();
    }

    private static void byte2hexAppend(byte[] bs, int off, int length, StringBuilder sb) {
        if (bs.length <= off || bs.length < off + length)
            throw new IllegalArgumentException();
        sb.ensureCapacity(sb.length() + length * 2);
        for (int i = off; i < off + length; i++) {
            sb.append(Character.forDigit(bs[i] >>> 4 & 0xf, 16));
            sb.append(Character.forDigit(bs[i] & 0xf, 16));
        }
    }
}
