package com.paydustry.register.aidl;

import android.os.Parcel;
import android.os.Parcelable;

public enum PrinterStatus implements Parcelable {
    OK,
    PAPER_END,
    NO_CONTENT,
    HARDWARE_ERROR,
    OVERHEAT,
    BUFFER_OVERFLOW,
    LOW_VOLTAGE,
    PAPER_PENDING,
    MOTOR_ERROR,
    PE_NOT_FOUND,
    PAPER_JAM,
    NOBM,
    BUSY,
    BMBLACK,
    WORK_ON,
    LIFT_HEAD,
    CUT_POSITION_ERROR,
    LOW_TEMPERATURE,
    OTHER
    ;

    public static final Creator<PrinterStatus> CREATOR = new Creator<PrinterStatus>() {
        @Override
        public PrinterStatus createFromParcel(Parcel in) {
            return PrinterStatus.fromInteger(in.readInt());
        }

        @Override
        public PrinterStatus[] newArray(int size) {
            return new PrinterStatus[size];
        }
    };
    
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(toInteger());
    }

    public int toInteger() { return this.ordinal(); }
    public static PrinterStatus fromInteger(int value) {
        return values()[value];
    }
}
