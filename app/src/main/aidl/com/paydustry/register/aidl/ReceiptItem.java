package com.paydustry.register.aidl;

import android.os.Parcel;
import android.os.Parcelable;

public class ReceiptItem implements Parcelable {
    protected String type;
    protected String data;
    protected AlignAttribute alignAttribute;
    protected SizeAttribute sizeAttribute;
    protected WeightAttribute weightAttribute;

    protected ReceiptItem(Class<? extends ReceiptItem> clazz, String data, AlignAttribute alignAttribute, SizeAttribute sizeAttribute, WeightAttribute weightAttribute) {
        this.type = clazz.getSimpleName();
        this.data = data;
        this.alignAttribute = alignAttribute;
        this.sizeAttribute = sizeAttribute;
        this.weightAttribute = weightAttribute;
    }

    public static final Creator<ReceiptItem> CREATOR = new Creator<ReceiptItem>() {
        @Override
        public ReceiptItem createFromParcel(Parcel in) {
            return new ReceiptItem(in);
        }

        @Override
        public ReceiptItem[] newArray(int size) {
            return new ReceiptItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(data);
        dest.writeParcelable(alignAttribute, flags);
        dest.writeParcelable(sizeAttribute, flags);
        dest.writeParcelable(weightAttribute, flags);
    }

    protected ReceiptItem(Parcel in) {
        type = in.readString();
        data = in.readString();
        alignAttribute = in.readParcelable(AlignAttribute.class.getClassLoader());
        sizeAttribute = in.readParcelable(SizeAttribute.class.getClassLoader());
        weightAttribute = in.readParcelable(WeightAttribute.class.getClassLoader());
    }

    public String getData() {
        return data;
    }
    public String getType() {
        return type;
    }

    public AlignAttribute getAlignAttribute() {
        return alignAttribute;
    }

    public SizeAttribute getSizeAttribute() {
        return sizeAttribute;
    }

    public WeightAttribute getWeightAttribute() {
        return weightAttribute;
    }
}
