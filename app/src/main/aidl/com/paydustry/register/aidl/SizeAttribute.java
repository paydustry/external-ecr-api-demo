package com.paydustry.register.aidl;

import android.os.Parcel;
import android.os.Parcelable;

public enum SizeAttribute implements Parcelable {
    small, normal, large
    ;

    public static final Creator<SizeAttribute> CREATOR = new Creator<SizeAttribute>() {
        @Override
        public SizeAttribute createFromParcel(Parcel in) {
            return SizeAttribute.fromInteger(in.readInt());
        }

        @Override
        public SizeAttribute[] newArray(int size) {
            return new SizeAttribute[size];
        }
    };
    
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(toInteger());
    }

    public int toInteger() { return this.ordinal(); }
    public static SizeAttribute fromInteger(int value) {
        return values()[value];
    }
}
