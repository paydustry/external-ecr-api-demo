package com.paydustry.register.aidl;

import android.os.Parcel;

public class TextItem extends ReceiptItem {
    /**
     * TextItem line to print on receipt.
     * Use this api to print text on a line.
     * @param text to be printed
     * @param alignAttribute to align the text
     * @param sizeAttribute to give the size
     * @param weightAttribute to set if the text should be bold
     */
    public TextItem(String text, AlignAttribute alignAttribute, SizeAttribute sizeAttribute, WeightAttribute weightAttribute) {
        super(TextItem.class, text, alignAttribute, sizeAttribute, weightAttribute);
    }

    /**
     * Constructor to be used for internal purposes
     */
    protected TextItem(Parcel in) {
        super(in);
    }

    /**
     * Constructor to be used for internal purposes
     */
    public TextItem(ReceiptItem receiptItem) {
        this(receiptItem.data, receiptItem.alignAttribute, receiptItem.sizeAttribute, receiptItem.weightAttribute);
    }
}
