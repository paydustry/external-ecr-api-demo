package com.paydustry.register.aidl;

import android.os.Parcel;
import android.os.Parcelable;

public enum WeightAttribute implements Parcelable {
    normal, bold
    ;

    public static final Creator<WeightAttribute> CREATOR = new Creator<WeightAttribute>() {
        @Override
        public WeightAttribute createFromParcel(Parcel in) {
            return WeightAttribute.fromInteger(in.readInt());
        }

        @Override
        public WeightAttribute[] newArray(int size) {
            return new WeightAttribute[size];
        }
    };
    
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(toInteger());
    }

    public int toInteger() { return this.ordinal(); }
    public static WeightAttribute fromInteger(int value) {
        return values()[value];
    }
}
