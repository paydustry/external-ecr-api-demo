package com.paydustry.register.extregisterapidemo;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.paydustry.register.aidl.AlignAttribute;
import com.paydustry.register.aidl.IRegisterAPI;
import com.paydustry.register.aidl.ImageItem;
import com.paydustry.register.aidl.ReceiptItem;
import com.paydustry.register.aidl.SizeAttribute;
import com.paydustry.register.aidl.TextItem;
import com.paydustry.register.aidl.WeightAttribute;
import com.paydustry.register.extregisterapidemo.utils.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private IRegisterAPI registerAPI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonTest = findViewById(R.id.button_print_Test);
        buttonTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    List<ReceiptItem> list = new ArrayList<>();
                    list.add(new TextItem("\n", AlignAttribute.center, SizeAttribute.normal, WeightAttribute.normal));
                    list.add(new TextItem("-------TEST-------", AlignAttribute.center, SizeAttribute.normal, WeightAttribute.normal));
                    list.add(new TextItem("\n", AlignAttribute.center, SizeAttribute.normal, WeightAttribute.normal));

                    list.add(new TextItem("Left normal normal", AlignAttribute.left, SizeAttribute.normal, WeightAttribute.normal));
                    list.add(new TextItem("Right normal normal", AlignAttribute.right, SizeAttribute.normal, WeightAttribute.normal));
                    list.add(new TextItem("Center normal normal", AlignAttribute.center, SizeAttribute.normal, WeightAttribute.normal));
                    list.add(new TextItem("Center Aligned || normal normal", AlignAttribute.center, SizeAttribute.normal, WeightAttribute.normal));

                    list.add(new TextItem("------------------", AlignAttribute.center, SizeAttribute.normal, WeightAttribute.normal));

                    list.add(new TextItem("Left small normal", AlignAttribute.left, SizeAttribute.small, WeightAttribute.normal));
                    list.add(new TextItem("Right small normal", AlignAttribute.right, SizeAttribute.small, WeightAttribute.normal));
                    list.add(new TextItem("Center small normal", AlignAttribute.center, SizeAttribute.small, WeightAttribute.normal));
                    list.add(new TextItem("Center Aligned || small normal", AlignAttribute.center, SizeAttribute.small, WeightAttribute.normal));

                    list.add(new TextItem("------------------", AlignAttribute.center, SizeAttribute.normal, WeightAttribute.normal));

                    list.add(new TextItem("Left large normal", AlignAttribute.left, SizeAttribute.large, WeightAttribute.normal));
                    list.add(new TextItem("Right large normal", AlignAttribute.right, SizeAttribute.large, WeightAttribute.normal));
                    list.add(new TextItem("Center large normal", AlignAttribute.center, SizeAttribute.large, WeightAttribute.normal));
                    list.add(new TextItem("Center Aligned || large normal", AlignAttribute.center, SizeAttribute.large, WeightAttribute.normal));

                    list.add(new TextItem("------------------", AlignAttribute.center, SizeAttribute.normal, WeightAttribute.normal));

                    list.add(new TextItem("Left normal bold", AlignAttribute.left, SizeAttribute.normal, WeightAttribute.bold));
                    list.add(new TextItem("Right normal bold", AlignAttribute.right, SizeAttribute.normal, WeightAttribute.bold));
                    list.add(new TextItem("Center normal bold", AlignAttribute.center, SizeAttribute.normal, WeightAttribute.bold));
                    list.add(new TextItem("Center Aligned || normal bold", AlignAttribute.center, SizeAttribute.normal, WeightAttribute.bold));

                    list.add(new TextItem("------------------", AlignAttribute.center, SizeAttribute.normal, WeightAttribute.normal));

                    list.add(new TextItem("Left small bold", AlignAttribute.left, SizeAttribute.small, WeightAttribute.bold));
                    list.add(new TextItem("Right small bold", AlignAttribute.right, SizeAttribute.small, WeightAttribute.bold));
                    list.add(new TextItem("Center small bold", AlignAttribute.center, SizeAttribute.small, WeightAttribute.bold));
                    list.add(new TextItem("Center Aligned || small bold", AlignAttribute.center, SizeAttribute.small, WeightAttribute.bold));

                    list.add(new TextItem("------------------", AlignAttribute.center, SizeAttribute.normal, WeightAttribute.normal));

                    list.add(new TextItem("\n", AlignAttribute.center, SizeAttribute.normal, WeightAttribute.normal));
                    InputStream in = getResources().openRawResource(R.raw.ic_banking_receipt_logo);
                    list.add(new ImageItem(in, AlignAttribute.center, SizeAttribute.normal, WeightAttribute.normal));

                    registerAPI.getPrinter().printWithFeed(list);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });

        Button buttonStatus = findViewById(R.id.button_printer_status);
        buttonStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Toast.makeText(MainActivity.this, registerAPI.getPrinter().getPrinterStatus().name(), Toast.LENGTH_SHORT).show();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
        Button buttonSocket = findViewById(R.id.button_connect_socket);
        buttonSocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String ip = Utils.getIPAddress(true);
                final Handler handler = new Handler();
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            final Socket s = new Socket(ip, 2000);
                            final boolean socketConnected = s.isConnected();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this, "Socket connected = " + socketConnected, Toast.LENGTH_SHORT).show();
                                }
                            });
                            s.close();
                        } catch (final IOException e) {
                            e.printStackTrace();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this, "Socket connection exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                });

                thread.start();
            }
        });
    }

    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            registerAPI = IRegisterAPI.Stub.asInterface(service);
            Log.i("MainActivity", "!!!!! onServiceConnected !!!!");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i("MainActivity", "!!!!! onServiceDisconnected !!!!");
        }
    };

    private boolean connectTMSService() {
        Log.i("MainActivity", "connectTMSService");

        final String ACTION = "com.paydustry.register.external.ExternalRegisterService";
        final String PACKAGE = "com.paydustry.register.external";
        final String App = getApplicationContext().getPackageName();
        Intent intent = new Intent();

        intent.putExtra("App", App);
        intent.setAction(ACTION);
        intent.setPackage(PACKAGE);

        boolean bret = getApplicationContext().bindService(intent, conn, Context.BIND_AUTO_CREATE);
        Log.i("MainActivity", "bind service bret = " + bret);
        return bret;
    }

    @Override
    protected void onResume() {
        Log.i("MainActivity", "onResume");
        connectTMSService();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        Log.i("MainActivity", "onDestroy");
        try {
            unbindService(conn);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }
}
